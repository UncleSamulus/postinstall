# My Fedora Ansible Post Install

## Quick Setup

1. Install  `ansible`

```{bash}
sudo dnf install ansible
```

2. Run the playbooks

```{bash}
ansible-playbook <playbook.yaml>
```

## References

Some playbooks forked from <https://github.com/jpjubenot/ansible-post-install-my-fedora> CC By Jacques-Philippe JUBENOT.